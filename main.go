package main

import (
	"archive/zip"
	"bufio"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"

	"fmt"
	"io"
	"sync"
	"time"
	"unicode"

	"bytes"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"reflect"
	"strconv"
)

type WeatherData struct {
	Name              string `json:"name"`
	Date              string `json:"date"`
	Time              string `json:"time"`
	Status            string `json:"status"`
	Visibility        string `json:"visibility"`
	Temperature       string `json:"temperature"`
	Thermal_sensation string `json:"thermal_sensation"`
	Wind              string `json:"wind"`
	Humidity          string `json:"humidity"`
	Pressure          string `json:"pressure"`
	Data_timestamp    string `json:"data_timestamp"`
}

// Declare a global variable to hold a timestamp
var (
	zipURL   = getEnvOrDefault("ZIP_URL", "https://ssl.smn.gob.ar/dpd/zipopendata.php?dato=tiepre")
    tmpDir   = getEnvOrDefault("TMP_DIR", "/tmp")
    interval = getEnvAsDuration("INTERVAL", 10 * time.Minute)
	globalTimestamp   time.Time = time.Now()
	globalWeatherData map[string]WeatherData
	mutex             sync.RWMutex
)

func getEnvOrDefault(envName string, defaultVal string) string {
    val := os.Getenv(envName)
    if val == "" {
        return defaultVal
    }
    return val
}

func getEnvAsDuration(envName string, defaultVal time.Duration) time.Duration {
    valStr := os.Getenv(envName)
    if valStr == "" {
        return defaultVal
    }

    valInt, err := strconv.ParseInt(valStr, 10, 64)
    if err != nil {
        fmt.Printf("Error parsing %s: %v. Using default value.\n", envName, err)
        return defaultVal
    }

    return time.Duration(valInt) * time.Second
}

func extractFile(f *zip.File, filePath string) error {
	rc, err := f.Open()
	if err != nil {
		return err
	}
	defer rc.Close()

	// Read the file content into a buffer
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(rc)
	if err != nil {
		return err
	}

	// Convert ISO-8859-1 to UTF-8
	utf8Data, err := convertISO88591ToUTF8(buf.Bytes())
	if err != nil {
		return err
	}

	// Write the UTF-8 encoded data to the file
	outFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	if err != nil {
		return err
	}
	defer outFile.Close()

	_, err = outFile.Write(utf8Data)
	return err
}

func convertISO88591ToUTF8(data []byte) ([]byte, error) {
	// Use the charmap.ISO8859_1 decoder directly on the data
	decoder := charmap.ISO8859_1.NewDecoder()
	utf8Data, _, err := transform.Bytes(decoder, data)
	if err != nil {
		return nil, err
	}

	return utf8Data, nil
}

func fetchAndUnzip() error {
	// Fetch the ZIP file
	resp, err := http.Get(zipURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create a temporary file
	tmpFile, err := os.CreateTemp(tmpDir, "*.zip")
	if err != nil {
		return err
	}
	defer tmpFile.Close()

	// Write the response body to the temporary file
	_, err = io.Copy(tmpFile, resp.Body)
	if err != nil {
		return err
	}
	println(tmpFile.Name())
	// Unzip the file
	zipReader, err := zip.OpenReader(tmpFile.Name())
	if err != nil {
		return err
	}
	defer zipReader.Close()

	file := zipReader.File[0]
	// Extract files to tmpDir
	fPath := filepath.Join(tmpDir, "data.txt")
	if file.FileInfo().IsDir() {
		os.MkdirAll(fPath, os.ModePerm)
	} else {
		if err := extractFile(file, fPath); err != nil {
			return err
		}
	}

	println(tmpFile.Name())
	os.Remove(tmpFile.Name())

	return nil
}

func updateSource() {
	var err error
	currentTime := time.Now()
	mutex.Lock()
	difference := currentTime.Sub(globalTimestamp)
	if difference > interval {
		fmt.Println(fmt.Sprintf("Difference curr - stored timestamp: %s / More than %s", difference,interval))
		err = fetchAndUnzip()

		if err != nil {
			println(err)
		}

		globalWeatherData, err = getWeatherData()

		if err != nil {
			println(err)
		}
		globalTimestamp = currentTime
		mutex.Unlock()
	} else {
		mutex.Unlock()
	}
}

func isMn(r rune) bool {
	return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
}

func removeAccents(input string) string {
	t := transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)
	result, _, err := transform.String(t, input)
	if err != nil {
		return input // Return the original string in case of an error
	}
	return result
}

func trimLastSpaceAndSlash(s string) string {
	// Remove trailing space and slash
	s = strings.TrimRight(s, " /")

	return s
}

func getWeatherData() (map[string]WeatherData, error) {

	file, err := os.Open(tmpDir+"/data.txt") // Replace 'data.txt' with your actual data file
	if err != nil {
		println(err)
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	weatherDataMap := make(map[string]WeatherData)

	for scanner.Scan() {
		record := scanner.Text()
		fields := strings.Split(record, ";")
		if len(fields) < 9 {
			continue // Skip if the record is incomplete
		}

		weatherData := WeatherData{
			Name:              strings.TrimSpace(fields[0]),
			Date:              strings.TrimSpace(fields[1]),
			Time:              strings.TrimSpace(fields[2]),
			Status:            strings.TrimSpace(fields[3]),
			Visibility:        strings.TrimSpace(fields[4]),
			Temperature:       strings.TrimSpace(fields[5]),
			Thermal_sensation: strings.TrimSpace(fields[6]),
			Humidity:          strings.TrimSpace(fields[7]),
			Wind:              strings.TrimSpace(fields[8]),
			Pressure:          trimLastSpaceAndSlash(strings.TrimSpace(fields[9])),
			Data_timestamp:    fmt.Sprintf("%s/%s",strings.TrimSpace(fields[1]),strings.TrimSpace(fields[2])),
		}

		city := strings.TrimSpace(fields[0])
		city = strings.Replace(city, " ", "_", -1) // Replace spaces with underscores
		city = removeAccents(city)                 // Remove accents
		city = strings.ToLower(city)               // Convert to lowercase
		println(city)

		weatherDataMap[city] = weatherData
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return weatherDataMap, nil
}

func selectFields(data interface{}, fields []string) (map[string]interface{}, error) {
    selectedData := make(map[string]interface{})
    val := reflect.ValueOf(data)

    // If a pointer is passed, we need to get the value it points to
    if val.Kind() == reflect.Ptr {
        val = val.Elem()
    }

    for _, field := range fields {
        valueField := val.FieldByName(field)
        if valueField.IsValid() { // Check if the field exists
            selectedData[strings.ToLower(field)] = valueField.Interface()
        } else {
            return nil, fmt.Errorf("field %s does not exist", field)
        }
    }

    return selectedData, nil
}

func query_data() map[string]WeatherData {
	updateSource()

	mutex.RLock() // Read lock
	weather := globalWeatherData
	mutex.RUnlock() // Release read lock
	return weather
}

func query_city(city string) (WeatherData, bool) {
	weather, ok := query_data()[city]
	return weather, ok
}

func main() {
	router := gin.Default()
	err := fetchAndUnzip()

	if err != nil {
		println(err)
	}

	// Compare the current time with the global timestamp
	currentTime := time.Now()
	difference := currentTime.Sub(globalTimestamp)

	fmt.Println("Global timestamp:", globalTimestamp)
	fmt.Println("Current time:", currentTime)
	fmt.Println("Difference:", difference)

	// Check if the difference is more than 10 minutes
	if difference > interval {
		fmt.Println("The difference is more than 10 minutes.")
	} else {
		fmt.Println("The difference is 10 minutes or less.")
	}

	globalWeatherData, err = getWeatherData()

	if err != nil {
		println(err)
	}

	router.GET("/data", func(c *gin.Context) {
		updateSource()

		weather := query_data()

		c.JSON(http.StatusOK, weather)
	})

	router.GET("/city", func(c *gin.Context) {
		city := c.Query("city") // Extract city name from query parameter

		weather, ok := query_city(city)

		if !ok {
			c.JSON(http.StatusNotFound, gin.H{"error": "City not found"})
			return
		}

		c.JSON(http.StatusOK, weather)
	})

	router.GET("/city_wind", func(c *gin.Context) {
		city := c.Query("city") // Extract city name from query parameter
		timestamp := c.Query("timestamp") // Extract timestamp query parameter
		fieldsToSelect := []string{"Wind"}

		weather, ok := query_city(city)

		if !ok {
			c.JSON(http.StatusNotFound, gin.H{"error": "City not found"})
			return
		}

		if timestamp == "true" {
			fieldsToSelect=append(fieldsToSelect, "Data_timestamp")
		}
		
		selected, err := selectFields(weather, fieldsToSelect)
    	if err != nil {
        	fmt.Println("Error:", err)
        	return
    	}
		c.JSON(http.StatusOK, selected)
	})

	router.GET("/city_temperature", func(c *gin.Context) {
		city := c.Query("city") // Extract city name from query parameter
		timestamp := c.Query("timestamp") // Extract timestamp query parameter
		fieldsToSelect := []string{"Temperature"}

		weather, ok := query_city(city)

		if !ok {
			c.JSON(http.StatusNotFound, gin.H{"error": "City not found"})
			return
		}

		if timestamp == "true" {
			fieldsToSelect=append(fieldsToSelect, "Data_timestamp")
		}
		
		selected, err := selectFields(weather, fieldsToSelect)
    	if err != nil {
        	fmt.Println("Error:", err)
        	return
    	}
		c.JSON(http.StatusOK, selected)
	})

	router.GET("/city_status", func(c *gin.Context) {
		city := c.Query("city") // Extract city name from query parameter
		timestamp := c.Query("timestamp") // Extract timestamp query parameter
		fieldsToSelect := []string{"Status"}

		weather, ok := query_city(city)

		if !ok {
			c.JSON(http.StatusNotFound, gin.H{"error": "City not found"})
			return
		}

		if timestamp == "true" {
			fieldsToSelect=append(fieldsToSelect, "Data_timestamp")
		}
		
		selected, err := selectFields(weather, fieldsToSelect)
    	if err != nil {
        	fmt.Println("Error:", err)
        	return
    	}
		c.JSON(http.StatusOK, selected)
	})

	router.GET("/city_data_timestamp", func(c *gin.Context) {
		city := c.Query("city") // Extract city name from query parameter

		weather, ok := query_city(city)

		if !ok {
			c.JSON(http.StatusNotFound, gin.H{"error": "City not found"})
			return
		}

		c.JSON(http.StatusOK, weather.Data_timestamp)
	})

	router.GET("/city_humidity", func(c *gin.Context) {
		city := c.Query("city") // Extract city name from query parameter
		timestamp := c.Query("timestamp") // Extract timestamp query parameter
		fieldsToSelect := []string{"Humidity"}

		weather, ok := query_city(city)

		if !ok {
			c.JSON(http.StatusNotFound, gin.H{"error": "City not found"})
			return
		}

		if timestamp == "true" {
			fieldsToSelect=append(fieldsToSelect, "Data_timestamp")
		}
		
		selected, err := selectFields(weather, fieldsToSelect)
    	if err != nil {
        	fmt.Println("Error:", err)
        	return
    	}
		c.JSON(http.StatusOK, selected)
	})

	router.GET("/city_pressure", func(c *gin.Context) {
		city := c.Query("city") // Extract city name from query parameter
		timestamp := c.Query("timestamp") // Extract timestamp query parameter
		fieldsToSelect := []string{"Pressure"}

		weather, ok := query_city(city)

		if !ok {
			c.JSON(http.StatusNotFound, gin.H{"error": "City not found"})
			return
		}

		if timestamp == "true" {
			fieldsToSelect=append(fieldsToSelect, "Data_timestamp")
		}
		
		selected, err := selectFields(weather, fieldsToSelect)
    	if err != nil {
        	fmt.Println("Error:", err)
        	return
    	}
		c.JSON(http.StatusOK, selected)
	})

	router.Run(":8080")
}
